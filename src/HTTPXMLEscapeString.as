package
{
	/**
	 HTTPXMLEscapeString
	 Part of AGORA - an interactive and web-based argument mapping tool that stimulates reasoning, 
	 reflection, critique, deliberation, and creativity in individual argument construction 
	 and in collaborative or adversarial settings. 
	 Copyright (C) 2011 Georgia Institute of Technology
	 
	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU Affero General Public License as
	 published by the Free Software Foundation, either version 3 of the
	 License, or (at your option) any later version.
	 
	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU Affero General Public License for more details.
	 
	 You should have received a copy of the GNU Affero General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
	 
	 */
	import flash.display.Sprite;
	
	public class HTTPXMLEscapeString extends Sprite
	{
		public function HTTPXMLEscapeString()
		{
			var oldstr:String="\"";
			var newstr:String = EscapeText(oldstr);
			trace("Old: " + oldstr + " New: " + newstr);
			oldstr="'";
			newstr=EscapeText(oldstr);
			trace("Old: " + oldstr + " New: " + newstr);
			oldstr="<";
			newstr=EscapeText(oldstr);
			trace("Old: " + oldstr + " New: " + newstr);
			oldstr=">";
			newstr=EscapeText(oldstr);
			trace("Old: " + oldstr + " New: " + newstr);
			oldstr="&";
			newstr=EscapeText(oldstr);
			trace("Old: " + oldstr + " New: " + newstr);
			oldstr=" ";
			newstr=EscapeText(oldstr);
			trace("Old: " + oldstr + " New: " + newstr);
			oldstr="This is a pointless & convoluted <string> designed specifically so that it'll " +
					"contain 100% of the \"special\" characters.";
			newstr=EscapeText(oldstr);
			trace("Old: " + oldstr + "\nNew: " + newstr);
			oldstr="'<\">& '<\">&";
			newstr=EscapeText(oldstr);
			trace("Old: " + oldstr + "\nNew: " + newstr);
			oldstr="testingIñtërnâtiônàlizætiøntesting漢字Сделанный%?\"&<>'testing";
			newstr=EscapeText(oldstr);
			trace("Old: " + oldstr + "\nNew: " + newstr);
		}
		
		public function EscapeText(s:String):String
		{
			s = s.replace(/%/g, "%25"); //must be first
			s = s.replace(/&/g, "&amp;");//must be before the various &s are added in
			s = s.replace(/\"/g, "&quot;");
			//s = s.replace(/'/g, "&apos;"); //This does not appear to be necessary
			s = s.replace(/</g, "&lt;");
			s = s.replace(/>/g, "&gt;");
			s = s.replace(/&/g, "%26"); //must be last
			return s;
		}
	}
}